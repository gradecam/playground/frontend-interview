import type { User } from "~/pages/index.vue"

interface DatabaseUser extends User {
    username: string,
    password: string
}

/**
 *  This is used as a mock database for the purposes of this demo.
 *  You should use this as a reference for the User type/interface in '~/pages/index.vue'
*/
export const useDatabase = (): DatabaseUser[] => {
    return [
        {
            username: 'admin',
            password: 'password',
            name: 'Rick Astley',
            roles: [
                {
                    name: 'admin',
                    permissions: [
                        'read',
                        'write',
                        'delete'
                    ],
                    resources: [
                        'users',
                        'posts',
                        'comments'
                    ],
                    level: 1
                }, {
                    name: 'dancer',
                    permissions: [
                        'read',
                        'write'
                    ],
                    resources: [
                        'posts',
                        'comments'
                    ],
                    level: 2
                }, {
                    name: 'singer',
                    permissions: [
                        'read'
                    ],
                    resources: [
                        'posts'
                    ],
                    level: 1
                }, {
                    name: 'actor',
                    permissions: null,
                    resources: [],
                    level: 3
                } , {
                    name: 'Egalitarian',
                    permissions: [],
                    resources: [],
                    level: 1
                }
            ],
            imgUri: 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.ym66ZEckaFWMDAzRy8VVVwHaE7%26pid%3DApi&f=1&ipt=f533322af9c8d00cd90a10d2c820accb790f8dcb05ce46507dac6f3b8bdb3aaf&ipo=images',
            tagline: 'Never gonna give you up',
            bio: "I'm looking for someone who doesn't take themselves too seriously, loves a good laugh, and won't mind me serenading them every now and then. If you're ready for a guy who's never gonna make you cry, say goodbye, tell a lie, or hurt you...then swipe right. 🎶",
            accessToken: '8675309'
        }
    ]
}
