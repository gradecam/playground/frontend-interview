import { useDatabase } from "~/composables/useDatabase"
import type { User } from "~/pages/index.vue"

type LoginBody = {
    username: string
    password: string
};

export default defineEventHandler(async (event) => {
    await new Promise(resolve => setTimeout(resolve, 1500))
    const body: LoginBody = await readBody(event)
    if (!body.username) {
        throw createError({
            statusCode: 400,
            statusMessage: "Username must not be empty"
        })
    }
    if (!body.password) {
        throw createError({
            statusCode: 400,
            statusMessage: "Password must not be empty"
        })
    }
    const user = useDatabase().find(user => user.username === body.username)
    if (!user) {
        throw createError({
            statusCode: 401,
            statusMessage: "Username not found"
        })
    } else if (user.password !== body.password) {
        throw createError({
            statusCode: 401,
            statusMessage: "Password is incorrect"
        })
    } else {
        const { password, username, ...userWithoutPasswordAndUsername } = user
        return userWithoutPasswordAndUsername as User
    }
})
