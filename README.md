# 🖐️ Welcome to the GradeCam Front End Software Engineer Candidate Repository! 💻

## 📋 Getting Started Guide and Important Notes

1. Fork and clone this repository.
2. Execute `npm install` command.
3. Execute `npm run dev` command.
4. Open `http://localhost:3000` in your web browser.

Notes:
- The primary file you will be modifying is 'index.vue' located in the 'pages' directory. You can find it [here](./pages/index.vue).
- You may use the Fetch API or Axios for making requests for this exercise.

## 🎯 Goals

As a group
---
- Review the overall structure of the application.

As an individual
---
- [ ] Finish the implementation of the 'login' method.
    - This should send a POST request to the '/api/auth/login' endpoint.
    - The request body should be a JSON object with the following properties:
        - username
        - password
    - The response from the the api request will contain the an object that represents the profile of the logged in user.
    - You should store the user object in the state variable.
    - If an error occurs during the request, you should:
        - set the error message returned by the response
        - Open the error dialog using the 'showModal' method on the native dialog.
- [ ] Define the 'User' type in the 'index.vue' file.
    - This type represents the data returned by the '/api/auth/login' endpoint.
- [ ] Update the rolePills computed property to:
    - Filter out any roles with no permissions.
    - Map the roles object to fit the structure of the 'pillRole' type.
        - each role has a 'level' and each level should be mapped to a matching color.
- [ ] Add CSS targeting the 'login-card' class to make the login card fill the screen when the viewport width is less than 600px.
